//
//  main.cpp
//  FindNullFiles
//
//  Created by Kevin H. Patterson on 2016-05-29.
//  Copyright © 2016 Kevin H. Patterson. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <vector>

using namespace std;
using namespace boost::filesystem;


bool IsNull( const path& p ) {
	bool Result = true;

	// 8192 * sizeof( uint64_t ) = 65536 bytes (64k)
	const size_t bufsize = 8192;
	typedef uint64_t buf_t;
	buf_t buf[bufsize];

	try {
//		boost::filesystem::ifstream infile;
//		infile.open( p, ios::binary | ios::in );
		std::ifstream infile;
		infile.open( p.string(), ios::binary | ios::in );

		infile.read( reinterpret_cast<char*>( &buf[0] ), bufsize * sizeof( buf_t ) );

		// scan the words for the first non-null (non-zero) word
		for( size_t i = 0; i < bufsize; ++ i ) {
			if( buf[i] ) {
				Result = false;
				break;
			}
		}

		infile.close();
	} catch( ... ) {
		cerr << "Exception C on " << p.generic_string() << endl;
	}

	return Result;
}


void Scan( const path& p ) {
//	cerr << "Scanning " << p.generic_string() << endl;

	vector<path> Directories;
	{
		vector<path> Files;
		try {
			for( directory_iterator it( p ); it != directory_iterator(); ++it ) {
				directory_entry& e = *it;
				try {
					// ignore hidden files
					if( e.path().filename().generic_string()[0] == '.' )
						continue;

					// ignore (don't follow) symlinks
					if( is_symlink( e ) )
						continue;

					if( is_regular_file( e ) )
						Files.push_back( e );

					if( is_directory( e ) )
						Directories.push_back( e );

				} catch( ... ) {
					cerr << "Exception B on " << e.path().generic_string() << endl;
				}
			}
		} catch( ... ) {
			cerr << "Exception A on " << p.generic_string() << endl;
		}

		for( auto File : Files ) {
			if( IsNull( File ) ) {
				cout << File.generic_string() << endl;
			}
		}
	}

	for( auto Directory : Directories )
		Scan( Directory );
}


int main( int argc, const char* argv[] ) {
	boost::filesystem::path p;

	if( argc > 1 )
		p = argv[1];
	else
		p = "/";

	p = canonical( p );

	cerr << "Scanning " << p.generic_string() << endl;

	Scan( p );

	return 0;
}
